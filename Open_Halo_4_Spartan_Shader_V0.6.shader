// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Open Halo 4/Open Halo 4 Spartan Shader V0.6"
{
	Properties
	{
		_ControlMap("Control Map", 2D) = "white" {}
		[Normal]_BumpMap("NormalMap", 2D) = "bump" {}
		[Toggle]_Halo4NormalMap("Halo 4 Normal Map?", Float) = 0
		_MetallicMultiplier("Metallic Multiplier", Range( 0 , 3)) = 1
		_SmoothnessMultiplier("Smoothness Multiplier ", Range( 0 , 1.25)) = 1
		[Toggle]_AmbientOcclusionSetValue("Ambient Occlusion Set Value?", Float) = 1
		_OcclusionStrength("AO Set Value:", Range( 0.45 , 1)) = 0.5
		_Color("Colour Red Channel", Color) = (1,0,0,0)
		_ColourGreenChannel("Colour Green Channel", Color) = (0.07799447,1,0,0)
		[Toggle]_ADDFakeZone3InverseofRGChannels("[ADD] Fake Zone 3? [Inverse of R&G Channels]", Float) = 0
		_FakeZone3Colour("Fake Zone 3 Colour", Color) = (1,1,1,0)
		_EmissionMap("[ADD] Emission Texture:", 2D) = "black" {}
		_EmissionColor("Emission Color", Color) = (0,0,0,0)
		_ADDRedDetailTexture("[ADD] Red Detail Texture:", 2D) = "white" {}
		_ADDGreenDetailTexture("[ADD] Green Detail Texture:", 2D) = "white" {}
		_ADDFakezone3DetailTexture("[ADD] Fake zone 3  Detail Texture:", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityCG.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _Halo4NormalMap;
		uniform sampler2D _BumpMap;
		uniform float4 _BumpMap_ST;
		uniform sampler2D _ControlMap;
		uniform float4 _ControlMap_ST;
		uniform float4 _Color;
		uniform sampler2D _ADDRedDetailTexture;
		uniform float4 _ADDRedDetailTexture_ST;
		uniform float4 _ColourGreenChannel;
		uniform sampler2D _ADDGreenDetailTexture;
		uniform float4 _ADDGreenDetailTexture_ST;
		uniform float _ADDFakeZone3InverseofRGChannels;
		uniform float4 _FakeZone3Colour;
		uniform sampler2D _ADDFakezone3DetailTexture;
		uniform float4 _ADDFakezone3DetailTexture_ST;
		uniform sampler2D _EmissionMap;
		uniform float4 _EmissionMap_ST;
		uniform float4 _EmissionColor;
		uniform float _MetallicMultiplier;
		uniform float _SmoothnessMultiplier;
		uniform float _AmbientOcclusionSetValue;
		uniform float _OcclusionStrength;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_BumpMap = i.uv_texcoord * _BumpMap_ST.xy + _BumpMap_ST.zw;
			float3 tex2DNode7 = UnpackNormal( tex2D( _BumpMap, uv_BumpMap ) );
			float3 appendResult14 = (float3(tex2DNode7.r , -tex2DNode7.g , tex2DNode7.b));
			o.Normal = (( _Halo4NormalMap )?( appendResult14 ):( tex2DNode7 ));
			float2 uv_ControlMap = i.uv_texcoord * _ControlMap_ST.xy + _ControlMap_ST.zw;
			float4 tex2DNode1 = tex2D( _ControlMap, uv_ControlMap );
			float ControlG73 = tex2DNode1.g;
			float Diffuse68 = ControlG73;
			float4 temp_cast_0 = (Diffuse68).xxxx;
			float3 temp_cast_1 = (tex2DNode1.a).xxx;
			float3 temp_cast_2 = (tex2DNode1.a).xxx;
			float3 linearToGamma527 = LinearToGammaSpace( temp_cast_2 );
			float3 ControlAlphaGamma535 = linearToGamma527;
			float3 temp_output_670_0 = ( ( ControlAlphaGamma535 - float3( 0.75,0.75,0.75 ) ) * float3( 4,4,4 ) );
			float3 clampResult728 = clamp( temp_output_670_0 , float3( 0,0,0 ) , float3( 1,1,1 ) );
			float3 Colour1714 = clampResult728;
			float2 uv_ADDRedDetailTexture = i.uv_texcoord * _ADDRedDetailTexture_ST.xy + _ADDRedDetailTexture_ST.zw;
			float4 colorZone1Detail166 = tex2D( _ADDRedDetailTexture, uv_ADDRedDetailTexture );
			float3 clampResult801 = clamp( ( ControlAlphaGamma535 - ( temp_output_670_0 * float3( 1,1,1 ) ) ) , float3( 0,0,0 ) , float3( 1,1,1 ) );
			float ControlAlpha123 = tex2DNode1.a;
			float3 clampResult803 = clamp( ( ( clampResult801 * ControlAlpha123 ) * float3( 2.5,2.5,2.5 ) ) , float3( 0,0,0 ) , float3( 1,1,1 ) );
			float3 Colour2715 = clampResult803;
			float2 uv_ADDGreenDetailTexture = i.uv_texcoord * _ADDGreenDetailTexture_ST.xy + _ADDGreenDetailTexture_ST.zw;
			float4 ColorZone2Detail168 = tex2D( _ADDGreenDetailTexture, uv_ADDGreenDetailTexture );
			float4 clampResult717 = clamp( ( ( float4( Colour1714 , 0.0 ) * ( _Color * colorZone1Detail166 ) ) + ( float4( Colour2715 , 0.0 ) * ( _ColourGreenChannel * ColorZone2Detail168 ) ) ) , float4( 0,0,0,0 ) , float4( 1,1,1,0 ) );
			float3 clampResult751 = clamp( ( 1.0 - ( Colour1714 + Colour2715 ) ) , float3( 0,0,0 ) , float3( 1,1,1 ) );
			float2 uv_ADDFakezone3DetailTexture = i.uv_texcoord * _ADDFakezone3DetailTexture_ST.xy + _ADDFakezone3DetailTexture_ST.zw;
			float4 ColorZone3Detail170 = tex2D( _ADDFakezone3DetailTexture, uv_ADDFakezone3DetailTexture );
			float4 blendOpSrc569 = temp_cast_0;
			float4 blendOpDest569 = ( clampResult717 + (( _ADDFakeZone3InverseofRGChannels )?( ( float4( clampResult751 , 0.0 ) * ( _FakeZone3Colour * ColorZone3Detail170 ) ) ):( float4( clampResult751 , 0.0 ) )) );
			float4 temp_cast_7 = (( 0.5 + 0.5 )).xxxx;
			float4 blendOpSrc658 = ( saturate( ( blendOpSrc569 * blendOpDest569 ) ));
			float4 blendOpDest658 = temp_cast_7;
			float4 FiysColourMethod97 = ( saturate( ( blendOpSrc658 * blendOpDest658 ) ));
			o.Albedo = FiysColourMethod97.rgb;
			float2 uv_EmissionMap = i.uv_texcoord * _EmissionMap_ST.xy + _EmissionMap_ST.zw;
			o.Emission = ( tex2D( _EmissionMap, uv_EmissionMap ) * _EmissionColor ).rgb;
			float ControlR67 = tex2DNode1.r;
			float clampResult571 = clamp( ( ControlR67 * _MetallicMultiplier ) , 0.0 , 1.0 );
			float VarMetallic428 = clampResult571;
			o.Metallic = VarMetallic428;
			float clampResult570 = clamp( ( ControlG73 * _SmoothnessMultiplier ) , 0.0 , 1.0 );
			float VarSmoothness429 = clampResult570;
			o.Smoothness = VarSmoothness429;
			float clampResult580 = clamp( (( _AmbientOcclusionSetValue )?( _OcclusionStrength ):( tex2DNode7.b )) , 0.0 , 1.0 );
			o.Occlusion = clampResult580;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Standard"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18100
-1920;207;1920;1018;1435.585;2016.578;1;True;True
Node;AmplifyShaderEditor.CommentaryNode;57;-496,-1536;Inherit;False;5743.567;2685.474;Colour Zones Metallics and Smoothness are all contained within the Control File. Red is Colour Zone 1, Green 2, Blue is the Halo 5 Metallic, And Alpha is the Halo 5 Smoothness;13;68;6;608;611;610;609;125;535;527;1;65;226;114;Diffuse & Control: Your Colouring Metallic and Smoothness Data;0,0.3379312,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;1;-481.0926,-1296;Inherit;True;Property;_ControlMap;Control Map;1;0;Create;True;0;0;False;0;False;-1;None;88e0dccafe4e2d44584f5b9bd523518a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LinearToGammaNode;527;-480,-1088;Inherit;True;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;711;-496,1312;Inherit;False;2421.441;743.3989;Make the Colour Zones;20;714;715;803;728;807;802;801;808;683;729;680;677;679;675;674;671;712;670;668;663;Seperate Colour Zones to Use in Open Halo Style;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;535;-256,-1088;Float;True;ControlAlphaGamma;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;663;-464,1360;Inherit;True;535;ControlAlphaGamma;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;668;-128,1360;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0.75,0.75,0.75;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;670;96,1360;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;4,4,4;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;712;320,1600;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;609;-208,-1168;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;610;-208,-1168;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;671;320,1600;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;674;-496,1584;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;611;176,-1056;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;125;240,-1440;Inherit;False;528.7554;750.0869;Spliting the Control into channels as intended for use;6;67;280;279;121;123;73;The Control Split;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;675;-496,1584;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;608;176,-1056;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;279;256,-880;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;679;-176,1680;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;677;-496,1680;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;680;-176,1680;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;280;272,-880;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;729;-448,1648;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;123;544,-912;Float;True;ControlAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;683;-144,1648;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;808;-144,1872;Inherit;False;123;ControlAlpha;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;801;64,1648;Inherit;True;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;802;304,1648;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;807;791,1648;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;2.5,2.5,2.5;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;65;-465,-576;Inherit;False;4641.533;1704.518;Fiy's Colouring Method, with Point's Detail textures:        Using the Control, and Defuse, Let's Get some Armor Colours Going                                 More subtle then inital method, prevents blown colours.;13;128;569;380;660;213;211;205;82;81;64;116;126;717;Colouring: The most visable part of the Shader;0.3529412,0.8929005,1,1;0;0
Node;AmplifyShaderEditor.ClampOpNode;803;1088,1648;Inherit;True;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ClampOpNode;728;368,1360;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;714;512,1360;Float;True;Colour1;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;126;207,640;Inherit;False;2261.492;419.854;Here the Red and Green Of the Control are Added, and then inverted, to create us the 3rd zone of the Armor's Colouring;10;96;94;526;174;120;661;751;754;753;752;THERE IS NO DEFAULT 3rd COLOUR ZONE;1,0.8482759,0,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;715;1568,1632;Float;True;Colour2;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;226;832,-1472;Inherit;False;664.4159;777.4517;Registering Detail Albedos;8;331;332;170;171;168;167;166;165;Detail Albedos;0.9926471,0.175173,0.93627,1;0;0
Node;AmplifyShaderEditor.SamplerNode;171;864,-1088;Inherit;True;Property;_ADDFakezone3DetailTexture;[ADD] Fake zone 3  Detail Texture:;16;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;754;576,688;Inherit;True;714;Colour1;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;752;784,752;Inherit;True;715;Colour2;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;56;-512,2272;Inherit;False;1784.802;516.9932;For Normal Maps, *NOTE* that the default format of Halo Normal Maps is for Direct X. Unity uses OPENGL Rendering, where the Green channel is used differently. INVERT GREEN CHANNEL TO FIX IT FOR UNITY!;15;11;14;7;138;220;221;223;224;247;248;251;252;301;302;511;Normals;0,0.6689658,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;170;1200,-1088;Float;True;ColorZone3Detail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;753;1136,688;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;7;-480,2320;Inherit;True;Property;_BumpMap;NormalMap;2;1;[Normal];Create;False;0;0;False;0;False;-1;None;d86877106257e5043ad0b16574d79d10;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;220;-80,2640;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;120;239,688;Float;False;Property;_FakeZone3Colour;Fake Zone 3 Colour;11;0;Create;True;0;0;False;0;False;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;174;224,864;Inherit;True;170;ColorZone3Detail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;526;1360,688;Inherit;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;320;4016,1888;Inherit;False;1132.072;1213.781;Where it's all coming Together;23;243;202;582;583;581;585;586;587;299;300;580;588;589;572;573;579;576;309;507;508;310;637;98;OUTPUT;0.7389706,0.9852941,0.9343306,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;202;4048,2944;Float;False;Property;_OcclusionStrength;AO Set Value:;7;0;Create;False;0;0;False;0;False;0.5;1;0.45;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;751;1552,688;Inherit;True;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;167;864,-1264;Inherit;True;Property;_ADDGreenDetailTexture;[ADD] Green Detail Texture:;15;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;165;864,-1440;Inherit;True;Property;_ADDRedDetailTexture;[ADD] Red Detail Texture:;14;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;661;576,944;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;223;-80,2640;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;166;1200,-1440;Float;True;colorZone1Detail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;511;-80,2640;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;94;1887,832;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;116;-449,-528;Inherit;False;618.8059;1581.453;Grabbing Data From Split & User Etc;9;172;79;119;173;66;118;69;209;210;;1,0,0,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;168;1200,-1264;Float;True;ColorZone2Detail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;585;4304,2928;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;248;-160,2688;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;73;544,-1152;Float;True;ControlG;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;119;-433,480;Float;False;Property;_ColourGreenChannel;Colour Green Channel;9;0;Create;True;0;0;False;0;False;0.07799447,1,0,0;0.8207547,0.1664738,0.1664738,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;581;4304,2928;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;224;112,2640;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;247;-160,2688;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;173;-433,656;Inherit;True;168;ColorZone2Detail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;118;-433,-96;Float;False;Property;_Color;Colour Red Channel;8;0;Create;False;0;0;False;0;False;1,0,0,0;0.3882352,0.3843137,0.3803921,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;172;-433,80;Inherit;True;166;colorZone1Detail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;96;2143,736;Float;True;Property;_ADDFakeZone3InverseofRGChannels;[ADD] Fake Zone 3? [Inverse of R&G Channels];10;0;Create;True;0;0;False;0;False;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;210;-112,480;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;79;-432,288;Inherit;True;715;Colour2;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;68;16,-1488;Float;False;Diffuse;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;67;352,-1392;Float;True;ControlR;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;583;4048,2832;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;66;-433,-288;Inherit;True;714;Colour1;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;251;1232,2688;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;718;864,448;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NegateNode;138;-48,2432;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;209;-128,-96;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;221;112,2640;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;114;1520,-1472;Inherit;False;3721.344;882.909;Hidden in the Control, and tossed by most, we're going to use the Red and Green Channels as Metallic and Smoothness, although Halo 4 is said to be sepecular, we are going to use metallic smoothness in this version of the shader.;9;428;429;636;21;100;124;20;122;99;Metallic And Smoothness;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;122;1536,-1392;Inherit;True;67;ControlR;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;124;1536,-944;Inherit;True;73;ControlG;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;14;256,2384;Inherit;True;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;69;-433,-496;Inherit;True;68;Diffuse;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;20;1744,-864;Float;False;Property;_SmoothnessMultiplier;Smoothness Multiplier ;5;0;Create;True;0;0;False;0;False;1;1;0;1.25;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;252;1232,2688;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;64;191,-144;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;81;207,352;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;719;864,448;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;99;1742.995,-1328;Float;False;Property;_MetallicMultiplier;Metallic Multiplier;4;0;Create;True;0;0;False;0;False;1;1;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;582;4048,2832;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;180;-512,2912;Inherit;False;1789.95;466.3504;Maps for lighting on the Armor;5;144;143;131;509;510;Emmission;0.7389706,0.9852941,0.9343306,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;636;4400,-1408;Inherit;False;604;697;These Clamps are here to Prevent Artifacting, due to Blown out Values;2;570;571;;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;299;4032,2656;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;82;464,240;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;11;736,2320;Float;True;Property;_Halo4NormalMap;Halo 4 Normal Map?;3;0;Create;True;0;0;False;0;False;0;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;721;848,304;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;587;4048,2704;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;2016,-944;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;211;1567,-448;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;100;2016,-1392;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;131;-480,2976;Inherit;True;Property;_EmissionMap;[ADD] Emission Texture:;12;0;Create;False;0;0;False;0;False;-1;None;None;True;0;False;black;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;213;1568,-448;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;660;2560,64;Inherit;False;889.7329;377.3918;These Ensure players don't look flat when shaders are hidden;4;657;656;658;659;Fallback Properties;1,0,0,1;0;0
Node;AmplifyShaderEditor.ClampOpNode;717;672,240;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;571;4560,-1328;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;586;4048,2704;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;720;848,304;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;143;-176,3136;Float;False;Property;_EmissionColor;Emission Color;13;0;Create;False;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;300;4032,2656;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;302;976,2352;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ClampOpNode;570;4560,-944;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;657;2688,320;Float;False;Constant;_Glossiness;Glossiness;19;0;Create;True;0;0;False;0;False;0.5;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;205;895,240;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;243;4080,2624;Float;True;Property;_AmbientOcclusionSetValue;Ambient Occlusion Set Value?;6;0;Create;True;0;0;False;0;False;1;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;301;976,2352;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;637;4048,2160;Inherit;False;485.7451;455.2663;Look In "Metallic And Smoothness" Section for Source;4;574;575;432;433;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;429;5024,-944;Float;False;VarSmoothness;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;144;-80,3008;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;380;1936,160;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;656;2688,224;Float;False;Constant;_Metallic;Metallic;19;0;Create;True;0;0;False;0;False;0.5;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;428;5040,-1328;Float;False;VarMetallic;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;580;4400,2624;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;510;1216,3040;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;303;1744,2352;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;432;4080,2208;Inherit;True;428;VarMetallic;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;433;4080,2416;Inherit;True;429;VarSmoothness;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;569;1968,128;Inherit;True;Multiply;True;3;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;659;2958,268;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;658;3104,128;Inherit;True;Multiply;True;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;588;4608,2656;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;575;4256,2240;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;305;1744,2352;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;128;3552,80;Inherit;False;478.1267;151.4682;Export Finished Colouring For Use Elsewhere in the Shader;1;97;;0,1,0.2551723,1;0;0
Node;AmplifyShaderEditor.WireNode;574;4288,2448;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;509;1216,3040;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;97;3680,128;Float;False;FiysColourMethod;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;309;4080,2000;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;589;4608,2640;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;573;4608,2208;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;579;4608,2176;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;507;4080,2032;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;576;4608,2176;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;6;-480,-1488;Inherit;True;Property;_MainTex;Diffuse;0;0;Create;False;0;0;False;0;False;-1;None;016c7a019013000459bf8247acdafb4a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;315;4768,2080;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;98;4032,1936;Inherit;False;97;FiysColourMethod;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;121;288,-1088;Float;True;ControlB;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;331;1200,-912;Float;True;VisorDetail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;310;4080,2000;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;332;864,-912;Inherit;True;Property;_ADDVisorDetailAlbedo;[ADD] Visor Detail Albedo;17;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;572;4608,2208;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;508;4080,2032;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;4800,1936;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Open Halo 4/Open Halo 4 Spartan Shader V0.6;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;Standard;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;253;-496,-1760;Inherit;False;361.9877;100;GNU LESSER GENERAL PUBLIC LICENSE                        Version 2.1, February 1999   Copyright (C) 1991, 1999 Free Software Foundation, Inc.  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  Everyone is permitted to copy and distribute verbatim copies  of this license document, but changing it is not allowed.  (This is the first released version of the Lesser GPL.  It also counts  as the successor of the GNU Library Public License, version 2, hence  the version number 2.1.)                              Preamble    The licenses for most software are designed to take away your freedom to share and change it.  By contrast, the GNU General Public Licenses are intended to guarantee your freedom to share and change free software--to make sure the software is free for all its users.    This license, the Lesser General Public License, applies to some specially designated software packages--typically libraries--of the Free Software Foundation and other authors who decide to use it.  You can use it too, but we suggest you first think carefully about whether this license or the ordinary General Public License is the better strategy to use in any particular case, based on the explanations below.    When we speak of free software, we are referring to freedom of use, not price.  Our General Public Licenses are designed to make sure that you have the freedom to distribute copies of free software (and charge for this service if you wish)  that you receive source code or can get it if you want it  that you can change the software and use pieces of it in new free programs  and that you are informed that you can do these things.    To protect your rights, we need to make restrictions that forbid distributors to deny you these rights or to ask you to surrender these rights.  These restrictions translate to certain responsibilities for you if you distribute copies of the library or if you modify it.    For example, if you distribute copies of the library, whether gratis or for a fee, you must give the recipients all the rights that we gave you.  You must make sure that they, too, receive or can get the source code.  If you link other code with the library, you must provide complete object files to the recipients, so that they can relink them with the library after making changes to the library and recompiling it.  And you must show them these terms so they know their rights.    We protect your rights with a two-step method: (1) we copyright the library, and (2) we offer you this license, which gives you legal permission to copy, distribute and/or modify the library.    To protect each distributor, we want to make it very clear that there is no warranty for the free library.  Also, if the library is modified by someone else and passed on, the recipients should know that what they have is not the original version, so that the original author's reputation will not be affected by problems that might be introduced by others.    Finally, software patents pose a constant threat to the existence of any free program.  We wish to make sure that a company cannot effectively restrict the users of a free program by obtaining a restrictive license from a patent holder.  Therefore, we insist that any patent license obtained for a version of the library must be consistent with the full freedom of use specified in this license.    Most GNU software, including some libraries, is covered by the ordinary GNU General Public License.  This license, the GNU Lesser General Public License, applies to certain designated libraries, and is quite different from the ordinary General Public License.  We use this license for certain libraries in order to permit linking those libraries into non-free programs.    When a program is linked with a library, whether statically or using a shared library, the combination of the two is legally speaking a combined work, a derivative of the original library.  The ordinary General Public License therefore permits such linking only if the entire combination fits its criteria of freedom.  The Lesser General Public License permits more lax criteria for linking other code with the library.    We call this license the "Lesser" General Public License because it does Less to protect the user's freedom than the ordinary General Public License.  It also provides other free software developers Less of an advantage over competing non-free programs.  These disadvantages are the reason we use the ordinary General Public License for many libraries.  However, the Lesser license provides advantages in certain special circumstances.    For example, on rare occasions, there may be a special need to encourage the widest possible use of a certain library, so that it becomes a de-facto standard.  To achieve this, non-free programs must be allowed to use the library.  A more frequent case is that a free library does the same job as widely used non-free libraries.  In this case, there is little to gain by limiting the free library to free software only, so we use the Lesser General Public License.    In other cases, permission to use a particular library in non-free programs enables a greater number of people to use a large body of free software.  For example, permission to use the GNU C Library in non-free programs enables many more people to use the whole GNU operating system, as well as its variant, the GNU/Linux operating system.    Although the Lesser General Public License is Less protective of the users' freedom, it does ensure that the user of a program that is linked with the Library has the freedom and the wherewithal to run that program using a modified version of the Library.    The precise terms and conditions for copying, distribution and modification follow.  Pay close attention to the difference between a "work based on the library" and a "work that uses the library".  The former contains code derived from the library, whereas the latter must be combined with the library in order to run.                    GNU LESSER GENERAL PUBLIC LICENSE    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION    0. This License Agreement applies to any software library or other program which contains a notice placed by the copyright holder or other authorized party saying it may be distributed under the terms of this Lesser General Public License (also called "this License"). Each licensee is addressed as "you".    A "library" means a collection of software functions and/or data prepared so as to be conveniently linked with application programs (which use some of those functions and data) to form executables.    The "Library", below, refers to any such software library or work which has been distributed under these terms.  A "work based on the Library" means either the Library or any derivative work under copyright law: that is to say, a work containing the Library or a portion of it, either verbatim or with modifications and/or translated straightforwardly into another language.  (Hereinafter, translation is included without limitation in the term "modification".)    "Source code" for a work means the preferred form of the work for making modifications to it.  For a library, complete source code means all the source code for all modules it contains, plus any associated interface definition files, plus the scripts used to control compilation and installation of the library.    Activities other than copying, distribution and modification are not covered by this License  they are outside its scope.  The act of running a program using the Library is not restricted, and output from such a program is covered only if its contents constitute a work based on the Library (independent of the use of the Library in a tool for writing it).  Whether that is true depends on what the Library does and what the program that uses the Library does.    1. You may copy and distribute verbatim copies of the Library's complete source code as you receive it, in any medium, provided that you conspicuously and appropriately publish on each copy an appropriate copyright notice and disclaimer of warranty  keep intact all the notices that refer to this License and to the absence of any warranty  and distribute a copy of this License along with the Library.    You may charge a fee for the physical act of transferring a copy, and you may at your option offer warranty protection in exchange for a fee.    2. You may modify your copy or copies of the Library or any portion of it, thus forming a work based on the Library, and copy and distribute such modifications or work under the terms of Section 1 above, provided that you also meet all of these conditions:      a) The modified work must itself be a software library.      b) You must cause the files modified to carry prominent notices     stating that you changed the files and the date of any change.      c) You must cause the whole of the work to be licensed at no     charge to all third parties under the terms of this License.      d) If a facility in the modified Library refers to a function or a     table of data to be supplied by an application program that uses     the facility, other than as an argument passed when the facility     is invoked, then you must make a good faith effort to ensure that,     in the event an application does not supply such function or     table, the facility still operates, and performs whatever part of     its purpose remains meaningful.      (For example, a function in a library to compute square roots has     a purpose that is entirely well-defined independent of the     application.  Therefore, Subsection 2d requires that any     application-supplied function or table used by this function must     be optional: if the application does not supply it, the square     root function must still compute square roots.)  These requirements apply to the modified work as a whole.  If identifiable sections of that work are not derived from the Library, and can be reasonably considered independent and separate works in themselves, then this License, and its terms, do not apply to those sections when you distribute them as separate works.  But when you distribute the same sections as part of a whole which is a work based on the Library, the distribution of the whole must be on the terms of this License, whose permissions for other licensees extend to the entire whole, and thus to each and every part regardless of who wrote it.  Thus, it is not the intent of this section to claim rights or contest your rights to work written entirely by you  rather, the intent is to exercise the right to control the distribution of derivative or collective works based on the Library.  In addition, mere aggregation of another work not based on the Library with the Library (or with a work based on the Library) on a volume of a storage or distribution medium does not bring the other work under the scope of this License.    3. You may opt to apply the terms of the ordinary GNU General Public License instead of this License to a given copy of the Library.  To do this, you must alter all the notices that refer to this License, so that they refer to the ordinary GNU General Public License, version 2, instead of to this License.  (If a newer version than version 2 of the ordinary GNU General Public License has appeared, then you can specify that version instead if you wish.)  Do not make any other change in these notices.    Once this change is made in a given copy, it is irreversible for that copy, so the ordinary GNU General Public License applies to all subsequent copies and derivative works made from that copy.    This option is useful when you wish to copy part of the code of the Library into a program that is not a library.    4. You may copy and distribute the Library (or a portion or derivative of it, under Section 2) in object code or executable form under the terms of Sections 1 and 2 above provided that you accompany it with the complete corresponding machine-readable source code, which must be distributed under the terms of Sections 1 and 2 above on a medium customarily used for software interchange.    If distribution of object code is made by offering access to copy from a designated place, then offering equivalent access to copy the source code from the same place satisfies the requirement to distribute the source code, even though third parties are not compelled to copy the source along with the object code.    5. A program that contains no derivative of any portion of the Library, but is designed to work with the Library by being compiled or linked with it, is called a "work that uses the Library".  Such a work, in isolation, is not a derivative work of the Library, and therefore falls outside the scope of this License.    However, linking a "work that uses the Library" with the Library creates an executable that is a derivative of the Library (because it contains portions of the Library), rather than a "work that uses the library".  The executable is therefore covered by this License. Section 6 states terms for distribution of such executables.    When a "work that uses the Library" uses material from a header file that is part of the Library, the object code for the work may be a derivative work of the Library even though the source code is not. Whether this is true is especially significant if the work can be linked without the Library, or if the work is itself a library.  The threshold for this to be true is not precisely defined by law.    If such an object file uses only numerical parameters, data structure layouts and accessors, and small macros and small inline functions (ten lines or less in length), then the use of the object file is unrestricted, regardless of whether it is legally a derivative work.  (Executables containing this object code plus portions of the Library will still fall under Section 6.)    Otherwise, if the work is a derivative of the Library, you may distribute the object code for the work under the terms of Section 6. Any executables containing that work also fall under Section 6, whether or not they are linked directly with the Library itself.    6. As an exception to the Sections above, you may also combine or link a "work that uses the Library" with the Library to produce a work containing portions of the Library, and distribute that work under terms of your choice, provided that the terms permit modification of the work for the customer's own use and reverse engineering for debugging such modifications.    You must give prominent notice with each copy of the work that the Library is used in it and that the Library and its use are covered by this License.  You must supply a copy of this License.  If the work during execution displays copyright notices, you must include the copyright notice for the Library among them, as well as a reference directing the user to the copy of this License.  Also, you must do one of these things:      a) Accompany the work with the complete corresponding     machine-readable source code for the Library including whatever     changes were used in the work (which must be distributed under     Sections 1 and 2 above)  and, if the work is an executable linked     with the Library, with the complete machine-readable "work that     uses the Library", as object code and/or source code, so that the     user can modify the Library and then relink to produce a modified     executable containing the modified Library.  (It is understood     that the user who changes the contents of definitions files in the     Library will not necessarily be able to recompile the application     to use the modified definitions.)      b) Use a suitable shared library mechanism for linking with the     Library.  A suitable mechanism is one that (1) uses at run time a     copy of the library already present on the user's computer system,     rather than copying library functions into the executable, and (2)     will operate properly with a modified version of the library, if     the user installs one, as long as the modified version is     interface-compatible with the version that the work was made with.      c) Accompany the work with a written offer, valid for at     least three years, to give the same user the materials     specified in Subsection 6a, above, for a charge no more     than the cost of performing this distribution.      d) If distribution of the work is made by offering access to copy     from a designated place, offer equivalent access to copy the above     specified materials from the same place.      e) Verify that the user has already received a copy of these     materials or that you have already sent this user a copy.    For an executable, the required form of the "work that uses the Library" must include any data and utility programs needed for reproducing the executable from it.  However, as a special exception, the materials to be distributed need not include anything that is normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which the executable runs, unless that component itself accompanies the executable.    It may happen that this requirement contradicts the license restrictions of other proprietary libraries that do not normally accompany the operating system.  Such a contradiction means you cannot use both them and the Library together in an executable that you distribute.    7. You may place library facilities that are a work based on the Library side-by-side in a single library together with other library facilities not covered by this License, and distribute such a combined library, provided that the separate distribution of the work based on the Library and of the other library facilities is otherwise permitted, and provided that you do these two things:      a) Accompany the combined library with a copy of the same work     based on the Library, uncombined with any other library     facilities.  This must be distributed under the terms of the     Sections above.      b) Give prominent notice with the combined library of the fact     that part of it is a work based on the Library, and explaining     where to find the accompanying uncombined form of the same work.    8. You may not copy, modify, sublicense, link with, or distribute the Library except as expressly provided under this License.  Any attempt otherwise to copy, modify, sublicense, link with, or distribute the Library is void, and will automatically terminate your rights under this License.  However, parties who have received copies, or rights, from you under this License will not have their licenses terminated so long as such parties remain in full compliance.    9. You are not required to accept this License, since you have not signed it.  However, nothing else grants you permission to modify or distribute the Library or its derivative works.  These actions are prohibited by law if you do not accept this License.  Therefore, by modifying or distributing the Library (or any work based on the Library), you indicate your acceptance of this License to do so, and all its terms and conditions for copying, distributing or modifying the Library or works based on it.    10. Each time you redistribute the Library (or any work based on the Library), the recipient automatically receives a license from the original licensor to copy, distribute, link with or modify the Library subject to these terms and conditions.  You may not impose any further restrictions on the recipients' exercise of the rights granted herein. You are not responsible for enforcing compliance by third parties with this License.    11. If, as a consequence of a court judgment or allegation of patent infringement or for any other reason (not limited to patent issues), conditions are imposed on you (whether by court order, agreement or otherwise) that contradict the conditions of this License, they do not excuse you from the conditions of this License.  If you cannot distribute so as to satisfy simultaneously your obligations under this License and any other pertinent obligations, then as a consequence you may not distribute the Library at all.  For example, if a patent license would not permit royalty-free redistribution of the Library by all those who receive copies directly or indirectly through you, then the only way you could satisfy both it and this License would be to refrain entirely from distribution of the Library.  If any portion of this section is held invalid or unenforceable under any particular circumstance, the balance of the section is intended to apply, and the section as a whole is intended to apply in other circumstances.  It is not the purpose of this section to induce you to infringe any patents or other property right claims or to contest validity of any such claims  this section has the sole purpose of protecting the integrity of the free software distribution system which is implemented by public license practices.  Many people have made generous contributions to the wide range of software distributed through that system in reliance on consistent application of that system  it is up to the author/donor to decide if he or she is willing to distribute software through any other system and a licensee cannot impose that choice.  This section is intended to make thoroughly clear what is believed to be a consequence of the rest of this License.    12. If the distribution and/or use of the Library is restricted in certain countries either by patents or by copyrighted interfaces, the original copyright holder who places the Library under this License may add an explicit geographical distribution limitation excluding those countries, so that distribution is permitted only in or among countries not thus excluded.  In such case, this License incorporates the limitation as if written in the body of this License.    13. The Free Software Foundation may publish revised and/or new versions of the Lesser General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.  Each version is given a distinguishing version number.  If the Library specifies a version number of this License which applies to it and "any later version", you have the option of following the terms and conditions either of that version or of any later version published by the Free Software Foundation.  If the Library does not specify a license version number, you may choose any version ever published by the Free Software Foundation.    14. If you wish to incorporate parts of the Library into other free programs whose distribution conditions are incompatible with these, write to the author to ask for permission.  For software which is copyrighted by the Free Software Foundation, write to the Free Software Foundation  we sometimes make exceptions for this.  Our decision will be guided by the two goals of preserving the free status of all derivatives of our free software and of promoting the sharing and reuse of software generally.                              NO WARRANTY    15. BECAUSE THE LIBRARY IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY FOR THE LIBRARY, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE LIBRARY "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE LIBRARY IS WITH YOU.  SHOULD THE LIBRARY PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.    16. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR REDISTRIBUTE THE LIBRARY AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE LIBRARY (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE LIBRARY TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.                       END OF TERMS AND CONDITIONS             How to Apply These Terms to Your New Libraries    If you develop a new library, and you want it to be of the greatest possible use to the public, we recommend making it free software that everyone can redistribute and change.  You can do so by permitting redistribution under these terms (or, alternatively, under the terms of the ordinary General Public License).    To apply these terms, attach the following notices to the library.  It is safest to attach them to the start of each source file to most effectively convey the exclusion of warranty  and each file should have at least the "copyright" line and a pointer to where the full notice is found.      shaders     Copyright (C) 2019 John W      This library is free software  you can redistribute it and/or     modify it under the terms of the GNU Lesser General Public     License as published by the Free Software Foundation  either     version 2.1 of the License, or (at your option) any later version.      This library is distributed in the hope that it will be useful,     but WITHOUT ANY WARRANTY  without even the implied warranty of     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     Lesser General Public License for more details.      You should have received a copy of the GNU Lesser General Public     License along with this library  if not, write to the Free Software     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301     USA  Also add information on how to contact you by electronic and paper mail.  You should also get your employer (if you work as a programmer) or your school, if any, to sign a "copyright disclaimer" for the library, if necessary.  Here is a sample  alter the names:    Yoyodyne, Inc., hereby disclaims all copyright interest in the   library `Frob' (a library for tweaking knobs) written by James Random   Hacker.    {signature of Ty Coon}, 1 April 1990   Ty Coon, President of Vice  That's all there is to it! ;0;This Shader is Licensed under the LGPL License Please read the associated Text File.;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;129;-480,-1968;Inherit;False;100;100;...;0;Created By FiyCsf For Use With Halo 4 Spartan Models;1,1,1,1;0;0
WireConnection;527;0;1;4
WireConnection;535;0;527;0
WireConnection;668;0;663;0
WireConnection;670;0;668;0
WireConnection;712;0;670;0
WireConnection;609;0;1;4
WireConnection;610;0;609;0
WireConnection;671;0;712;0
WireConnection;674;0;671;0
WireConnection;611;0;610;0
WireConnection;675;0;674;0
WireConnection;608;0;611;0
WireConnection;279;0;608;0
WireConnection;679;0;663;0
WireConnection;677;0;675;0
WireConnection;680;0;679;0
WireConnection;280;0;279;0
WireConnection;729;0;677;0
WireConnection;123;0;280;0
WireConnection;683;0;680;0
WireConnection;683;1;729;0
WireConnection;801;0;683;0
WireConnection;802;0;801;0
WireConnection;802;1;808;0
WireConnection;807;0;802;0
WireConnection;803;0;807;0
WireConnection;728;0;670;0
WireConnection;714;0;728;0
WireConnection;715;0;803;0
WireConnection;170;0;171;0
WireConnection;753;0;754;0
WireConnection;753;1;752;0
WireConnection;220;0;7;3
WireConnection;526;0;753;0
WireConnection;751;0;526;0
WireConnection;661;0;120;0
WireConnection;661;1;174;0
WireConnection;223;0;220;0
WireConnection;166;0;165;0
WireConnection;511;0;223;0
WireConnection;94;0;751;0
WireConnection;94;1;661;0
WireConnection;168;0;167;0
WireConnection;585;0;202;0
WireConnection;248;0;7;3
WireConnection;73;0;1;2
WireConnection;581;0;585;0
WireConnection;224;0;511;0
WireConnection;247;0;248;0
WireConnection;96;0;751;0
WireConnection;96;1;94;0
WireConnection;210;0;119;0
WireConnection;210;1;173;0
WireConnection;68;0;73;0
WireConnection;67;0;1;1
WireConnection;583;0;581;0
WireConnection;251;0;247;0
WireConnection;718;0;96;0
WireConnection;138;0;7;2
WireConnection;209;0;118;0
WireConnection;209;1;172;0
WireConnection;221;0;224;0
WireConnection;14;0;7;1
WireConnection;14;1;138;0
WireConnection;14;2;221;0
WireConnection;252;0;251;0
WireConnection;64;0;66;0
WireConnection;64;1;209;0
WireConnection;81;0;79;0
WireConnection;81;1;210;0
WireConnection;719;0;718;0
WireConnection;582;0;583;0
WireConnection;299;0;252;0
WireConnection;82;0;64;0
WireConnection;82;1;81;0
WireConnection;11;0;7;0
WireConnection;11;1;14;0
WireConnection;721;0;719;0
WireConnection;587;0;582;0
WireConnection;21;0;124;0
WireConnection;21;1;20;0
WireConnection;211;0;69;0
WireConnection;100;0;122;0
WireConnection;100;1;99;0
WireConnection;213;0;211;0
WireConnection;717;0;82;0
WireConnection;571;0;100;0
WireConnection;586;0;587;0
WireConnection;720;0;721;0
WireConnection;300;0;299;0
WireConnection;302;0;11;0
WireConnection;570;0;21;0
WireConnection;205;0;717;0
WireConnection;205;1;720;0
WireConnection;243;0;300;0
WireConnection;243;1;586;0
WireConnection;301;0;302;0
WireConnection;429;0;570;0
WireConnection;144;0;131;0
WireConnection;144;1;143;0
WireConnection;380;0;213;0
WireConnection;428;0;571;0
WireConnection;580;0;243;0
WireConnection;510;0;144;0
WireConnection;303;0;301;0
WireConnection;569;0;380;0
WireConnection;569;1;205;0
WireConnection;659;0;656;0
WireConnection;659;1;657;0
WireConnection;658;0;569;0
WireConnection;658;1;659;0
WireConnection;588;0;580;0
WireConnection;575;0;432;0
WireConnection;305;0;303;0
WireConnection;574;0;433;0
WireConnection;509;0;510;0
WireConnection;97;0;658;0
WireConnection;309;0;305;0
WireConnection;589;0;588;0
WireConnection;573;0;574;0
WireConnection;579;0;575;0
WireConnection;507;0;509;0
WireConnection;576;0;579;0
WireConnection;315;0;589;0
WireConnection;121;0;1;3
WireConnection;331;0;332;0
WireConnection;310;0;309;0
WireConnection;572;0;573;0
WireConnection;508;0;507;0
WireConnection;0;0;98;0
WireConnection;0;1;310;0
WireConnection;0;2;508;0
WireConnection;0;3;576;0
WireConnection;0;4;572;0
WireConnection;0;5;315;0
ASEEND*/
//CHKSM=B3CF0780C7C37B330364242CD9DD44A8E67E2C6F